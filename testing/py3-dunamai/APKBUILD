# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-dunamai
pkgver=1.21.0
pkgrel=0
pkgdesc="Dynamic versioning library and CLI"
url="https://github.com/mtkennerly/dunamai"
arch="noarch"
license="MIT"
depends="py3-packaging"
makedepends="py3-gpep517 py3-poetry-core py3-wheel py3-installer"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/mtkennerly/dunamai/releases/download/v$pkgver/dunamai-$pkgver.tar.gz"
builddir="$srcdir/dunamai-$pkgver"
options="!check" # tests require a git repo

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
7f22fc442a419e6952a7754c51133d7d0e68c154fa34095bf033f0e44e96c096080c9f89bd5328584c5127114ebe37142c292a3e0b724bf294ce1752d87406b2  dunamai-1.21.0.tar.gz
"
