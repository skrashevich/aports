# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=tree-sitter-cli
pkgver=0.22.5
pkgrel=0
pkgdesc="Tree Sitter CLI"
url="https://tree-sitter.github.io/"
arch="all"
license="MIT"
makedepends="
	cargo
	cargo-auditable
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/tree-sitter/tree-sitter/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/tree-sitter-$pkgver"
options="!check" # fail in this release

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked

	sh ./script/fetch-fixtures
}

build() {
	cargo auditable build -p tree-sitter-cli --frozen --release
}

check() {
	cargo test -p tree-sitter-cli --frozen
}

package() {
	install -Dm755 target/release/tree-sitter -t "$pkgdir"/usr/bin/
}

sha512sums="
cff3902e8f6e0211b6d4fafecf9f0d73a2bf73efed68df0d6e8798aac3e685cbcc882fb698ebc10115e72bcf46b59aabd0b14dd402a08d3b42cfceffea5aab2c  tree-sitter-cli-0.22.5.tar.gz
"
